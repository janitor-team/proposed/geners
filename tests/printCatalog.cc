#include <iostream>
#include <fstream>

#include "test_utils.hh"

#include "geners/ContiguousCatalog.hh"
#include "geners/CatalogIO.hh"

using namespace gs;
using namespace std;

int main(int argc, char const* argv[])
{
    if (argc != 2)
    {
        cerr << "Usage: " << parse_progname(argv[0]) << " filename" << endl;
        return 1;
    }

    ifstream in(argv[1], ios_base::binary);
    if (!in.is_open())
    {
        cerr << "Error: failed to open file \"" << argv[1] << "\""
             << std::endl;
        return 1;
    }

    unsigned compressionCode = 0, mergeLevel = 0;
    std::vector<std::string> annotations;
    CPP11_auto_ptr<ContiguousCatalog> cat(readBinaryCatalog<ContiguousCatalog>(
        in, &compressionCode, &mergeLevel, &annotations, true));
    if (!cat.get())
    {
        cerr << "Error: failed to read catalog from file \"" << argv[1] << "\""
             << std::endl;
        return 1;
    }

    cout << "Compression code: " << compressionCode << endl;
    cout << "Merge level: " << mergeLevel << endl;
    for (unsigned i=0; i<annotations.size(); ++i)
        cout << "\nAnnotation " << i << ": " << annotations[i] << endl;

    const unsigned long long sz = cat->size();
    for (unsigned long long id=1; id<=sz; ++id)
    {
        CPP11_shared_ptr<const CatalogEntry> e = cat->retrieveEntry(id);
        cout << '\n';
        e->humanReadable(cout);
    }

    return 0;
}
