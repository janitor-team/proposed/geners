#ifndef TEST_UTILS_HH_
#define TEST_UTILS_HH_

#include <vector>
#include <cassert>
#include <string>

const char *parse_progname(const char *c);
int parse_int(const char *c, int *result);
int parse_unsigned(const char *c, unsigned *result);
int parse_double(const char *c, double *result);
int parse_vector_of_doubles(char **c, int n, std::vector<double>* v);

double test_rng();
std::string random_string(unsigned len);

// The following returns the time of run (wall clock) of a certain
// function in usec/cycle. n cycles are run during the measurement.
double time_of_run(void (*fcn)(void), unsigned n);

#endif // TEST_UTILS_HH_
